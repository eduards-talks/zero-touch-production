<!-- .slide: data-state="no-toc-progress" --> <!-- don't show toc progress bar on this slide -->

## Zero Touch Production
### The heck?
<!-- .element: class="no-toc-progress" --> <!-- slide not in toc progress bar -->


----  ----


## What is that all about

- Make production more reliable
- Make production more secure
- Emphasize principles and (automated) procedure
- Be compliant by design
- Move as fast as possible (guarded by policy)

----

## But Why

<img src="/images/typos.jpg">

----

## But Why

<img src="/images/insider_threat.jpg">

----

## But Why

<img src="/images/what_could_go_wrong.jpg">

----

## But How

Every change to a production environment must either be:

- made by automation (backed by tests)
- prevalidated by software
- made via an audited break-glass mechanism

----

## Two Major Ideas

- Reliable Automation
- Minimal APIs (through safe proxies)

----  ----

## Reliable Automation

----

## Least Privilege

- Only allow the actions required to get the concrete work item done
- Limit affectable assets
- Limit performable actions
- Limit time of access
- Ideally: tie this to a justification (e.g. customer/user request/ticket)

----

## With Guardrails

- Prevent changes to assets currently receiving user traffic
- Critical system status checks prior to performing changes
- Global production freeze during outages (except incident response team)
- Prevent stacking change effects (2 changes when done in parallel result in 100% production reboot)

----

## As Fast as Possible (Protected by Policy)

- Always build your automation to act at maximum speed
- Always add a rate limit
- Always add business rule limits (e.g. only do change between 0400 and 0600)
- Make the limits easy to change
- Never change all of production at once

----  ----

## Minimal APIs

----

## With Great Power...

- scaling
- changing configuration
- changing software
- changing state
- access data
- ...comes great temptation: all this can easily be used by a malicious actor

All of this assumes one thing full, unfettered access to the underlying system.

----

## Simple Example

- Healthy oncall rotation of 8 engineers for a given service
  - 2 oncallers at a time, oncall 1/3 of the time or less; 1 site, N+1 per site
- Number of people with potentially dangerous access scales linearly with services
  - 100 services => 800 SREs
- Assume that a mistake leading to a major outage has a 0.1% probability


----

## Safe Proxies to the Rescue

- Full audit log
- Fine grained authorization
- Rate Limiting
- Multi-Party Authorization (think code review)
- Decreases Exposed API
- Separates Policy from Pathway

----

## Potential Pitfalls

- There are new moving parts: We need an audited break-glass for emergency response
- Approval Mechanism down:
  - Escalate to service team
  - Request unilateral operation with justification (and have a review meeting)
- Proxy dependency issues
  - Escalate to service team
  - Enable break-glass mechanism, store audit trail locally and ship later (with review)
- Proxy unavailable
  - Work with temporary membership in production roles (timed to incident response rotation)

----  ----

## Summary

- Audit everything
- Rate-Limit everything
- Scope-Limit everything
- Remove unilateral privileged access

----  ----

## Thank you for listening
